package org.kllbff.edu.notificator;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.kllbff.edu.notificator.dao.Database;

public class Activity extends AppCompatActivity {
    public static final String PERSON_KEY = "user-id";
    public static final int USER_REQUEST_CODE  = 0x001;

    public static final int RESULT_CREATED = -2;
    public static final int RESULT_SAVED   = -3;
    public static final int RESULT_DELETED = -4;

    protected Database database;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = Database.getInstance();
        database.connect();
    }

    protected void prepareToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
