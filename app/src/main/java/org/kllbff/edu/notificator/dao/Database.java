package org.kllbff.edu.notificator.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.kllbff.edu.notificator.models.DatabaseBirthDate;
import org.kllbff.edu.notificator.models.DatabasePerson;
import org.kllbff.edu.notificator.models.Person;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    private static final String TAG_NAME = Database.class.getSimpleName();

    private static final String DATABASE_NAME = "birth_date_notificator.db";
    private static final int DATABASE_VERSION = 2;

    private static Database instance;

    public static Database getInstance() {
        return instance;
    }

    public static void newInstance(Context context) {
        instance = new Database(context);
    }

    private PersonTable personTable;
    private BirthDateTable birthDateTable;
    private SQLiteDatabase currentConnection;

    private Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        personTable = new PersonTable();
        birthDateTable = new BirthDateTable();
        Log.d(TAG_NAME, "Table providers instantiated");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        Log.d(TAG_NAME, "Creating tables");
        personTable.create(database);
        birthDateTable.create(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.d(TAG_NAME, "Upgrading tables from " + oldVersion + " up to " + newVersion);
        personTable.drop(database);
        birthDateTable.drop(database);
        personTable.create(database);
        birthDateTable.create(database);
    }

    public List<Person> allPersons() {
        ArrayList<Person> personList = new ArrayList<>();

        List<DatabasePerson> databasePersonList = personTable.all();
        for(DatabasePerson databasePerson : databasePersonList) {
            final DatabaseBirthDate databaseBirthDate = birthDateTable.forPerson(databasePerson.getId());
            Person person = new Person(databasePerson, databaseBirthDate);
            personList.add(person);
            Log.d(TAG_NAME, "Person (" + databasePerson.getId() + ") has BirthDate (" + databaseBirthDate.getId() + ")");
        }

        return personList;
    }

    public List<Person> birthDatesAt(int day, int month) {
        ArrayList<Person> personList = new ArrayList<>();

        List<DatabaseBirthDate> databaseBirthDateList = birthDateTable.at(day, month);
        for(DatabaseBirthDate databaseBirthDate : databaseBirthDateList) {
            DatabasePerson databasePerson = personTable.forId(databaseBirthDate.getPersonId());
            Person person = new Person(databasePerson, databaseBirthDate);
            personList.add(person);
        }

        return personList;
    }

    public List<Person> search(String text) {
        ArrayList<Person> personList = new ArrayList<>();

        List<DatabasePerson> databasePersonList = personTable.search(text);
        for(DatabasePerson databasePerson : databasePersonList) {
            DatabaseBirthDate databaseBirthDate = birthDateTable.forPerson(databasePerson.getId());
            Person person = new Person(databasePerson, databaseBirthDate);
            personList.add(person);
        }

        return personList;
    }

    public void add(Person person) {
        personTable.add(person.getDatabasePerson());
        birthDateTable.add(person.getDatabaseBirthDate());
    }

    public void update(Person person) {
        personTable.update(person.getDatabasePerson());
        birthDateTable.update(person.getDatabaseBirthDate());
    }

    public void delete(Person person) {
        personTable.delete(person.getId());
        birthDateTable.delete(person.getBirthDateId());
    }

    public void connect() {
        disconnect();

        currentConnection = getWritableDatabase();
        personTable.setDatabase(currentConnection);
        birthDateTable.setDatabase(currentConnection);
    }

    public void disconnect() {
        if(currentConnection == null) {
            return;
        }

        currentConnection.close();
    }
}
