package org.kllbff.edu.notificator.list;

import org.kllbff.edu.notificator.models.Person;

public interface PersonItemClickListener {
    public void onPersonItemClick(Person person);
}
