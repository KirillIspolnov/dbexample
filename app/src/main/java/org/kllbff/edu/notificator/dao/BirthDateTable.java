package org.kllbff.edu.notificator.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.kllbff.edu.notificator.models.DatabaseBirthDate;

import java.util.ArrayList;
import java.util.List;

public class BirthDateTable {
    private static final String TAG_NAME = BirthDateTable.class.getSimpleName();

    private static final String TABLE_NAME = "birth_dates";

    private static final String ID_COLUMN_NAME = "id";
    private static final int ID_COLUMN_INDEX = 0;

    private static final String PERSON_COLUMN_NAME = "person";
    private static final int PERSON_COLUMN_INDEX = 1;

    private static final String YEAR_COLUMN_NAME = "year";
    private static final int YEAR_COLUMN_INDEX = 2;

    private static final String MONTH_COLUMN_NAME = "month";
    private static final int MONTH_COLUMN_INDEX = 3;

    private static final String DAY_COLUMN_NAME = "day";
    private static final int DAY_COLUMN_INDEX = 4;

    private static final String IGNORE_YEAR_COLUMN_NAME = "ignore_year";
    private static final int IGNORE_YEAR_COLUMN_INDEX = 5;

    /**
     * CREATE TABLE IF NOT EXISTS birth_dates
     * (id TEXT, person TEXT, year INTEGER, month INTEGER, day INTEGER, ignore_year INTEGER, PRIMARY KEY(id, person))
     */
    private static final String CREATE_QUERY = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            ID_COLUMN_NAME + " TEXT, " + PERSON_COLUMN_NAME + " TEXT, " +
            YEAR_COLUMN_NAME + " INTEGER, " + MONTH_COLUMN_NAME + " INTEGER, " + DAY_COLUMN_NAME +
            " INTEGER, " + IGNORE_YEAR_COLUMN_NAME + " INTEGER, PRIMARY KEY (" + ID_COLUMN_NAME +
            ", " + PERSON_COLUMN_NAME + "))";

    /**
     * SELECT id, person, year, month, day, ignore_year
     * FROM birth_dates
     */
    private static final String SELECT_QUERY = "SELECT " + ID_COLUMN_NAME + ", " +
            PERSON_COLUMN_NAME + ", " + YEAR_COLUMN_NAME + ", " + MONTH_COLUMN_NAME + ", " +
            DAY_COLUMN_NAME + ", " + IGNORE_YEAR_COLUMN_NAME +
            " FROM " + TABLE_NAME;

    /**
     * DROP TABLE IF EXISTS birth_dates
     */
    public static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;

    private SQLiteDatabase database;

    protected BirthDateTable() { }

    public void setDatabase(SQLiteDatabase database) {
        this.database = database;
        Log.d(TAG_NAME, "BirthDateTable is ready");
    }

    public void create(SQLiteDatabase database) {
        Log.d(TAG_NAME, "Executing " + CREATE_QUERY);
        database.execSQL(CREATE_QUERY);
    }

    private DatabaseBirthDate parse(Cursor cursor) {
        DatabaseBirthDate birthDate = new DatabaseBirthDate();

        birthDate.setId(cursor.getString(ID_COLUMN_INDEX));
        birthDate.setPersonId(cursor.getString(PERSON_COLUMN_INDEX));
        birthDate.setYear(cursor.getInt(YEAR_COLUMN_INDEX));
        birthDate.setMonth(cursor.getInt(MONTH_COLUMN_INDEX));
        birthDate.setDay(cursor.getInt(DAY_COLUMN_INDEX));
        birthDate.setYearIgnored(cursor.getInt(IGNORE_YEAR_COLUMN_INDEX) == 1);

        Log.d(TAG_NAME, "Found: " + birthDate);
        return birthDate;
    }

    private Cursor select(String where) {
        String query = SELECT_QUERY;
        if(where != null && !where.isEmpty()) {
            query += " WHERE " + where;
        }
        
        Log.d(TAG_NAME, "Executing " + query);
        return database.rawQuery(query, null);
    }

    public DatabaseBirthDate forPerson(String personId) {
        Cursor cursor = select(PERSON_COLUMN_NAME + "='" + personId + "'");
        if(cursor.getCount() == 0) {
            Log.d(TAG_NAME, "No birth date for person (" + personId + ")");
            return null;
        }

        cursor.moveToFirst();
        DatabaseBirthDate birthDate = parse(cursor);
        cursor.close();
        return birthDate;
    }

    private ContentValues wrap(DatabaseBirthDate birthDate) {
        ContentValues row = new ContentValues();

        row.put(ID_COLUMN_NAME, birthDate.getId());
        row.put(PERSON_COLUMN_NAME, birthDate.getPersonId());
        row.put(YEAR_COLUMN_NAME, birthDate.getYear());
        row.put(MONTH_COLUMN_NAME, birthDate.getMonth());
        row.put(DAY_COLUMN_NAME, birthDate.getDay());
        row.put(IGNORE_YEAR_COLUMN_NAME, birthDate.isYearIgnored() ? 1 : 0);

        return row;
    }

    public List<DatabaseBirthDate> at(int day, int month) {
        StringBuilder where = new StringBuilder();

        if(day > 0 && day < 32) {
            where.append(DAY_COLUMN_NAME).append("=").append(day);
        }

        if(month > 0 && month < 13) {
            where.append(MONTH_COLUMN_NAME).append("=").append(month);
        }

        Cursor cursor = select(where.toString());
        if(cursor.getCount() == 0) {
            Log.d(TAG_NAME, "No birth date at (" + day + "." + month + ")");
            return new ArrayList<>();
        }

        cursor.moveToFirst();
        ArrayList<DatabaseBirthDate> list = new ArrayList<>();
        for(int i = 0; i < cursor.getCount(); i++) {
            list.add(parse(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public void add(DatabaseBirthDate birthDate) {
        Log.d(TAG_NAME, "Inserting " + birthDate);
        database.insert(TABLE_NAME, null, wrap(birthDate));
    }

    public void update(DatabaseBirthDate birthDate) {
        Log.d(TAG_NAME, "Updating " + birthDate);
        database.update(TABLE_NAME, wrap(birthDate), ID_COLUMN_NAME + "=?", new String[]{ birthDate.getId() });
    }

    public void delete(String id) {
        Log.d(TAG_NAME, "Deleting (" + id + ")");
        database.delete(TABLE_NAME, ID_COLUMN_NAME + "=?", new String[]{ id });
    }

    public void drop(SQLiteDatabase database) {
        Log.d(TAG_NAME, "Executing " + DROP_QUERY);
        database.execSQL(DROP_QUERY);
    }
}
