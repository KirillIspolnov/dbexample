package org.kllbff.edu.notificator.list;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.kllbff.edu.notificator.R;
import org.kllbff.edu.notificator.models.Person;

public class PersonViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG_NAME = PersonViewHolder.class.getSimpleName();

    private TextView surnameView;
    private TextView nameView;
    private TextView patronymicView;
    private TextView birthDateView;
    private Person person;

    public PersonViewHolder(@NonNull View view, @NonNull PersonItemClickListener listener) {
        super(view);

        surnameView = view.findViewById(R.id.surname);
        nameView = view.findViewById(R.id.name);
        patronymicView = view.findViewById(R.id.patronymic);
        birthDateView = view.findViewById(R.id.birthDate);

        view.setOnClickListener((item) -> {
            listener.onPersonItemClick(person);
        });
    }

    public void show(Person person) {
        Log.d(TAG_NAME, "Showing " + person);
        this.person = person;

        nameView.setText(person.getName());

        String surname = person.getSurname();
        if(surname.isEmpty()) {
            surnameView.setVisibility(View.GONE);
        } else {
            surnameView.setText(surname);
            surnameView.setVisibility(View.VISIBLE);
        }

        String patronymic = person.getPatronymic();
        if(patronymic.isEmpty()) {
            patronymicView.setVisibility(View.GONE);
        } else {
            patronymicView.setText(patronymic);
            patronymicView.setVisibility(View.VISIBLE);
        }

        StringBuilder birthDate = new StringBuilder();
        birthDate.append(person.getBirthDay())
                .append("-")
                .append(person.getBirthMonth());
        if(!person.isBirthYearIgnored()) {
            birthDate.append("-").append(person.getBirthYear());
        }
        birthDateView.setText(birthDate.toString());
    }
}
