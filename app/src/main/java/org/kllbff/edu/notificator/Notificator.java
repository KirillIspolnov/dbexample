package org.kllbff.edu.notificator;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import org.kllbff.edu.notificator.dao.Database;
import org.kllbff.edu.notificator.models.Person;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class Notificator extends Service {
    private Database database;
    private Random random;
    private NotificationManager notificationManager;
    private Handler handler;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        database = Database.getInstance();
        database.connect();

        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        random = new Random();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(handler == null) {
            handler = new Handler(Looper.getMainLooper());
            delayUpdate(1000 * 60);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private class UpdateTask implements Runnable {
        public void run() {
            long delay = 1000 * 60 * 15;

            Calendar calendar = Calendar.getInstance();
            if (calendar.get(Calendar.HOUR_OF_DAY) == 10) {
                List<Person> persons = database.birthDatesAt(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1);

                for (Person person : persons) {
                    sendNotification(person);
                }

                delay = 1000 * 60 * 60 * 20;
            }

            Log.d("Notificator", "Check");
            delayUpdate(delay);
        }
    }

    private void delayUpdate(long delay) {
        handler.postDelayed(new UpdateTask(), delay);
    }

    private void sendNotification(Person person) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.add)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Сегодня день рождения у " + person.getSurname() + " " + person.getName())
                .setTicker("Сегодня день рождения!")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true);
        Notification notification = builder.build();
        notificationManager.notify(random.nextInt(), notification);
    }
}
