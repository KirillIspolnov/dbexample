package org.kllbff.edu.notificator.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.UUID;

public class DatabasePerson implements Parcelable {
    private String id;
    private String name;
    private String surname;
    private String patronymic;

    public DatabasePerson() {
        this.id = UUID.randomUUID().toString();
        this.name = null;
        this.surname = null;
        this.patronymic = null;
    }

    public static final Creator<DatabasePerson> CREATOR = new Creator<DatabasePerson>() {
        @Override
        public DatabasePerson createFromParcel(Parcel in) {
            DatabasePerson person = new DatabasePerson();

            person.id = in.readString();
            person.name = in.readString();
            person.surname = in.readString();
            person.patronymic = in.readString();

            return person;
        }

        @Override
        public DatabasePerson[] newArray(int size) {
            return new DatabasePerson[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode(); //TODO magic
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(patronymic);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    @Override
    @NonNull
    public String toString() {
        return "DatabasePerson (" + id + ") " + surname + " " + name + (patronymic == null ? "" : " " + patronymic);
    }
}
