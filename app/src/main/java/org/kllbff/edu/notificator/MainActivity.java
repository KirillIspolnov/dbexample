package org.kllbff.edu.notificator;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.kllbff.edu.notificator.list.PersonListAdapter;
import org.kllbff.edu.notificator.models.Person;

import java.util.List;

public class MainActivity extends Activity {
    private RecyclerView userList;
    private EditText searchInput;
    private TextView notFoundView;

    private List<Person> oldPersons;
    private List<Person> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareToolbar();

        userList = findViewById(R.id.list);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            userList.setLayoutManager(new LinearLayoutManager(this));
        } else {
            userList.setLayoutManager(new GridLayoutManager(this, 2));
        }

        searchInput = findViewById(R.id.search_input);
        notFoundView = findViewById(R.id.not_found);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((View view) -> {
            startUserActivity(null);
        });
    }

    private void startUserActivity(Person person) {
        Intent intent = new Intent(getApplicationContext(), UserActivity.class);

        if(person != null) {
            intent.putExtra(PERSON_KEY, person);
        }

        startActivityForResult(intent, USER_REQUEST_CODE);
    }

    @Override
    public void onResume() {
        super.onResume();

        persons = database.allPersons();
        updateRecycleView();
        startService(new Intent(this, Notificator.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(oldPersons != null) {
            List<Person> temp = oldPersons;
            oldPersons = persons;
            persons = oldPersons;
            updateRecycleView();
        }
    }

    public void search(View view) {
        //save old results!
        oldPersons = persons;
        persons = database.search(searchInput.getText().toString());
        updateRecycleView();
    }

    private void updateRecycleView() {
        if(persons.isEmpty()) {
            notFoundView.setVisibility(View.VISIBLE);
        } else {
            notFoundView.setVisibility(View.GONE);
        }

        PersonListAdapter adapter = new PersonListAdapter(getLayoutInflater(), persons, this::startUserActivity);
        userList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void showSnackbar(int stringResource) {
        int background = ResourcesCompat.getColor(getResources(), R.color.snackbar_background, getTheme());
        Snackbar snackbar = Snackbar.make(userList, stringResource, Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(background);

        int color = ResourcesCompat.getColor(getResources(), R.color.text_light, getTheme());
        TextView snackbarText = snackbarView.findViewById(R.id.snackbar_text);
        snackbarText.setTextColor(color);

        snackbar.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == USER_REQUEST_CODE) {
            switch(resultCode) {
                case RESULT_CANCELED: showSnackbar(R.string.warning_cancel); break;
                case RESULT_CREATED:  showSnackbar(R.string.record_created); break;
                case RESULT_SAVED:    showSnackbar(R.string.record_saved);   break;
                case RESULT_DELETED:  showSnackbar(R.string.record_deleted); break;
            }
        }
    }
}
