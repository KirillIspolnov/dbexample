package org.kllbff.edu.notificator.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.UUID;

public class DatabaseBirthDate implements Parcelable {
    private String id;
    private String personId;
    private int year;
    private int month;
    private int day;
    private boolean ignoreYear;

    public DatabaseBirthDate() {
        this.id = UUID.randomUUID().toString();
        this.personId = null;
        this.year = -1;
        this.month = -1;
        this.day = -1;
        this.ignoreYear = false;
    }

    public static final Creator<DatabaseBirthDate> CREATOR = new Creator<DatabaseBirthDate>() {
        @Override
        public DatabaseBirthDate createFromParcel(Parcel parcel) {
            DatabaseBirthDate birthDate = new DatabaseBirthDate();

            birthDate.id = parcel.readString();
            birthDate.personId = parcel.readString();
            birthDate.year = parcel.readInt();
            birthDate.month = parcel.readInt();
            birthDate.day = parcel.readInt();
            birthDate.ignoreYear = parcel.readInt() != 0;

            return birthDate;
        }

        @Override
        public DatabaseBirthDate[] newArray(int size) {
            return new DatabaseBirthDate[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();//TODO magic
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(personId);
        parcel.writeInt(year);
        parcel.writeInt(month);
        parcel.writeInt(day);
        parcel.writeInt(ignoreYear ? 1 : 0);
    }

    public String getId() {
        return id;
    }

    public String getPersonId() {
        return personId;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public boolean isYearIgnored() {
        return ignoreYear;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setYearIgnored(boolean ignoreYear) {
        this.ignoreYear = ignoreYear;
    }

    @Override
    @NonNull
    public String toString() {
        return "DatabaseBirthDate (" + id + ") of person (" + personId + ") at " + year + "-" + month + "-" + day + (ignoreYear ? ": year ignored" : "");
    }
}
