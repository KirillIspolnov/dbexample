package org.kllbff.edu.notificator.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable {
    private DatabasePerson person;
    private DatabaseBirthDate birthDate;

    public Person(DatabasePerson person, DatabaseBirthDate birthDate) {
        this.person = person;
        this.birthDate = birthDate;
    }

    public Person() {
        this(new DatabasePerson(), new DatabaseBirthDate());
        birthDate.setPersonId(person.getId());
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(person, flags);
        parcel.writeParcelable(birthDate, flags);
    }

    @Override
    public int describeContents() {
        return hashCode(); //TODO magic
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel parcel) {
            ClassLoader classLoader = Person.class.getClassLoader();
            DatabasePerson person = parcel.readParcelable(classLoader);
            DatabaseBirthDate birthDate = parcel.readParcelable(classLoader);

            return new Person(person, birthDate);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public DatabasePerson getDatabasePerson() {
        return person;
    }

    public DatabaseBirthDate getDatabaseBirthDate() {
        return birthDate;
    }

    public String getId() {
        return person.getId();
    }

    public String getBirthDateId() {
        return birthDate.getId();
    }

    public String getName() {
        return person.getName();
    }

    public void setName(String name) {
        person.setName(name);
    }

    public String getSurname() {
        return person.getSurname();
    }

    public void setSurname(String surname) {
        person.setSurname(surname);
    }

    public String getPatronymic() {
        return person.getPatronymic();
    }

    public void setPatronymic(String patronymic) {
        person.setPatronymic(patronymic);
    }

    public int getBirthDay() {
        return birthDate.getDay();
    }

    public void setBirthDay(int day) {
        birthDate.setDay(day);
    }

    public int getBirthMonth() {
        return birthDate.getMonth();
    }

    public void setBirthMonth(int month) {
        birthDate.setMonth(month);
    }

    public int getBirthYear() {
        return birthDate.getYear();
    }

    public void setBirthYear(int year) {
        birthDate.setYear(year);
    }

    public boolean isBirthYearIgnored() {
        return birthDate.isYearIgnored();
    }

    public void setBirthYearIgnored(boolean ignored) {
        birthDate.setYearIgnored(ignored);
    }
}
