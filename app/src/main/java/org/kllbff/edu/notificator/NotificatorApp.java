package org.kllbff.edu.notificator;

import android.app.Application;
import android.util.Log;

import org.kllbff.edu.notificator.dao.Database;

public class NotificatorApp extends Application {
    public static final String TAG_NAME = NotificatorApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG_NAME, "Creating new Database instance");
        Database.newInstance(getApplicationContext());
    }
}
