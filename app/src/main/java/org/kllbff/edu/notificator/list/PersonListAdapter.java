package org.kllbff.edu.notificator.list;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.kllbff.edu.notificator.R;
import org.kllbff.edu.notificator.models.Person;

import java.util.List;

public class PersonListAdapter extends RecyclerView.Adapter<PersonViewHolder> {
    private static final String TAG_NAME = PersonListAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<Person> persons;
    private PersonItemClickListener listener;

    public PersonListAdapter(LayoutInflater inflater, List<Person> persons, PersonItemClickListener listener) {
        this.inflater = inflater;
        this.persons = persons;
        this.listener = listener;
        Log.d(TAG_NAME, "Created adapter: " + persons.toString());
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG_NAME, "Creating view holder");
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new PersonViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        Log.d(TAG_NAME, "Binding view holder for #" + position);
        Person person = persons.get(position);
        holder.show(person);
    }

    @Override
    public int getItemCount() {
        Log.d(TAG_NAME, "Requested count: " + persons.size());
        return persons.size();
    }

}
