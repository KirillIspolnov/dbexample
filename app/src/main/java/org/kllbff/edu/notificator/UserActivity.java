package org.kllbff.edu.notificator;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.kllbff.edu.notificator.models.Person;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class UserActivity extends Activity {
    private EditText nameView, surnameView, patronymicView;
    private DatePicker birthDatePicker;
    private CheckBox ignoreYear;

    private Person person;
    private boolean isPersonNew;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        prepareToolbar();

        nameView = findViewById(R.id.name);
        surnameView = findViewById(R.id.surname);
        patronymicView = findViewById(R.id.patronymic);
        birthDatePicker = findViewById(R.id.birthDate);
        ignoreYear = findViewById(R.id.ignore_year);

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            birthDatePicker.setMinDate(simpleDateFormat.parse("1900-01-01").getTime());
        } catch(Throwable t) {
            Log.e("UserActivity", "Fail", t);
        }
        birthDatePicker.setMaxDate(System.currentTimeMillis());

        FloatingActionButton saveButton = findViewById(R.id.save);
        FloatingActionButton delButton = findViewById(R.id.delete);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            person = extras.getParcelable(PERSON_KEY);
        }

        isPersonNew = (person == null);
        if(isPersonNew) {
            person = new Person();
            delButton.hide();
        } else {
            nameView.setText(person.getName());
            surnameView.setText(person.getSurname());
            patronymicView.setText(person.getPatronymic());
            birthDatePicker.init(person.getBirthYear(), person.getBirthMonth() - 1, person.getBirthDay(), null);
            ignoreYear.setChecked(person.isBirthYearIgnored());
        }
    }

    public void save(View view) {
        String name = nameView.getText().toString();

        if(name.isEmpty()) {
            nameView.setError(getString(R.string.empty_name));
            return;
        }

        person.setName(name);
        person.setSurname(surnameView.getText().toString());
        person.setPatronymic(patronymicView.getText().toString());
        person.setBirthYear(birthDatePicker.getYear());
        person.setBirthMonth(birthDatePicker.getMonth() + 1);
        person.setBirthDay(birthDatePicker.getDayOfMonth());
        person.setBirthYearIgnored(ignoreYear.isChecked());

        if (isPersonNew){
            Log.d("User", "A: " + person.getId() + " : " + person.getBirthDateId());
            database.add(person);
            goHome(RESULT_CREATED);
        } else {
            Log.d("User", "U: " + person.getId() + " : " + person.getBirthDateId());
            database.update(person);
            goHome(RESULT_SAVED);
        }
    }

    public void delete(View view){
        database.delete(person);
        goHome(RESULT_DELETED);
    }

    private void goHome(int result) {
        setResult(result);
        finish();
    }
}
