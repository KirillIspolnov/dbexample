package org.kllbff.edu.notificator.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.kllbff.edu.notificator.models.DatabasePerson;

import java.util.ArrayList;
import java.util.List;

public class PersonTable {
    private static final String TAG_NAME = PersonTable.class.getSimpleName();

    private static final String TABLE_NAME = "persons";

    private static final String ID_COLUMN_NAME = "id";
    private static final int ID_COLUMN_INDEX = 0;

    private static final String NAME_COLUMN_NAME = "name";
    private static final int NAME_COLUMN_INDEX = 1;

    private static final String SURNAME_COLUMN_NAME = "surname";
    private static final int SURNAME_COLUMN_INDEX = 2;

    private static final String PATRONYMIC_COLUMN_NAME = "patronymic";
    private static final int PATRONYMIC_COLUMN_INDEX = 3;

    /**
     * CREATE TABLE IF NOT EXISTS persons
     * (id TEXT, name TEXT, surname TEXT, patronymic TEXT, PRIMARY KEY(id))
     */
    private static final String CREATE_QUERY = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            ID_COLUMN_NAME + " TEXT, " + NAME_COLUMN_NAME + " TEXT, " + SURNAME_COLUMN_NAME + ", " +
            PATRONYMIC_COLUMN_NAME + " TEXT, PRIMARY KEY(" + ID_COLUMN_NAME + "))";

    /**
     * SELECT id, name, surname, patronymic
     * FROM persons
     */
    private static final String SIMPLE_SELECT_QUERY = "SELECT " + ID_COLUMN_NAME + ", " +
            NAME_COLUMN_NAME + ", " + SURNAME_COLUMN_NAME + ", " + PATRONYMIC_COLUMN_NAME +
            " FROM " + TABLE_NAME;

    /**
     * SELECT id, name, surname, patronymic
     * FROM persons
     * WHERE name LIKE '{key}%' OR surname LIKE '{key}%'
     */
    private static final String SEARCH_QUERY = "SELECT " + ID_COLUMN_NAME + ", " +
            NAME_COLUMN_NAME + ", " + SURNAME_COLUMN_NAME + ", " + PATRONYMIC_COLUMN_NAME +
            " FROM " + TABLE_NAME +
            " WHERE " + NAME_COLUMN_NAME + " LIKE '{key}%' OR " + SURNAME_COLUMN_NAME + " LIKE '{key}%'";

    /**
     * DROP TABLE IF EXISTS persons
     */
    public static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;

    private SQLiteDatabase database;

    protected PersonTable() { }

    public void setDatabase(SQLiteDatabase database) {
        this.database = database;
        Log.d(TAG_NAME, "PersonTable is ready to work");
    }

    public void create(SQLiteDatabase database) {
        Log.d(TAG_NAME, "Executing " + CREATE_QUERY);
        database.execSQL(CREATE_QUERY);
    }

    private DatabasePerson parse(Cursor cursor) {
        DatabasePerson person = new DatabasePerson();

        person.setId(cursor.getString(ID_COLUMN_INDEX));
        person.setName(cursor.getString(NAME_COLUMN_INDEX));
        person.setSurname(cursor.getString(SURNAME_COLUMN_INDEX));
        person.setPatronymic(cursor.getString(PATRONYMIC_COLUMN_INDEX));

        Log.d(TAG_NAME, "Found: " + person);
        return person;
    }

    private Cursor select(String query) {
        Log.d(TAG_NAME, "Executing " + query);
        return database.rawQuery(query, null);
    }

    public DatabasePerson forId(String id) {
        Cursor cursor = select(SIMPLE_SELECT_QUERY + " WHERE " + ID_COLUMN_NAME + "='" + id + "'");
        if(cursor.getCount() == 0) {
            Log.d(TAG_NAME, "No person (" + id + ")");
            return null;
        }

        cursor.moveToFirst();
        return parse(cursor);
    }

    public List<DatabasePerson> search(String text) {
        Cursor cursor = select(SEARCH_QUERY.replaceAll("\\{key\\}", text));
        if(cursor.getCount() == 0) {
            Log.d(TAG_NAME, "No persons with name or surname, containing " + text);
            return new ArrayList<>(0);
        }

        Log.d(TAG_NAME, "Found  " + cursor.getCount() + " persons with name or surname, containing " + text);
        cursor.moveToFirst();
        ArrayList<DatabasePerson> list = new ArrayList<>();
        for(int i = 0; i < cursor.getCount(); i++) {
            list.add(parse(cursor));
            cursor.moveToNext();
        }

        return list;
    }

    private ContentValues wrap(DatabasePerson person) {
        ContentValues row = new ContentValues();

        row.put(ID_COLUMN_NAME, person.getId());
        row.put(NAME_COLUMN_NAME, person.getName());
        row.put(SURNAME_COLUMN_NAME, person.getSurname());
        row.put(PATRONYMIC_COLUMN_NAME, person.getPatronymic());

        return row;
    }

    public void add(DatabasePerson person) {
        Log.d(TAG_NAME, "Inserting " + person);
        database.insertOrThrow(TABLE_NAME, null, wrap(person));
    }

    public void update(DatabasePerson person) {
        Log.d(TAG_NAME, "Updating " + person);
        database.update(TABLE_NAME, wrap(person), ID_COLUMN_NAME + "=?", new String[]{ person.getId() });
    }

    public void delete(String id) {
        Log.d(TAG_NAME, "Deleting (" + id + ")");
        database.delete(TABLE_NAME, ID_COLUMN_NAME + "=?", new String[]{ id });
    }

    public List<DatabasePerson> all() {
        Cursor cursor = select(SIMPLE_SELECT_QUERY);
        if(cursor.getCount() == 0) {
            Log.d(TAG_NAME, "No persons");
            return new ArrayList<>(0);
        }

        Log.d(TAG_NAME, "Found  " + cursor.getCount() + " persons");
        cursor.moveToNext();
        ArrayList<DatabasePerson> list = new ArrayList<>();
        for(int i = 0; i < cursor.getCount(); i++) {
            list.add(parse(cursor));
            cursor.moveToNext();
        }

        return list;
    }

    public void drop(SQLiteDatabase database) {
        Log.d(TAG_NAME, "Executing " + DROP_QUERY);
        database.execSQL(DROP_QUERY);
    }
}
